﻿using System;
using NUnit.Framework;
using Xamarin.UITest;

namespace QAAutomation
{
    [TestFixture(Platform.Android)]

    public abstract class BaseTestFixture
    {
        protected IApp app => AppManager.App;
        protected bool OnAndroid => AppManager.Platform == Platform.Android;

        protected BaseTestFixture(Platform platform)
        {
            AppManager.Platform = platform;
        }

        [SetUp]
        public virtual void BeforeEachTest()
        {
            AppManager.StartApp();
        }

        /*protected void EnterTask()
        {
            new TaskListPage()
                .VerifyUserClicksDisplay();

            new TaskListPage()
                .TestInputAccept();

            new TaskListPage()
                .PruebaBtn();




        }*/

    }
}

