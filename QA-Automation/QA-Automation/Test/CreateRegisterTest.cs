﻿using NUnit.Framework;
using QAAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
namespace QAAutomation
{
    public class CreateRegisterTest : BaseTestFixture
    {
        public CreateRegisterTest(Platform platform) : base(platform) { }

        [Test]
        public void CreateSuccess()
        {
            Assert.IsTrue(true, "Enter all the values", new CreateRegisterPage().CreateUser("User2","User2@gmail.com","12345678","12345678"));
        }

        [Test]
        public void CreateEmpty()
        {
            Assert.IsEmpty("", "It can't be empty, enter all the value",new CreateRegisterPage().CreateUser("","","",""));
            // Assert.IsTrue(true, "Enter all the values, it can't be empty", new CreateRegisterPage().CreateUser("", "", "", ""));
        }

        [Test]
        public void CreatePasswordsNotSame()
        {
            Assert.IsTrue(true, "Both password need to be the same", new CreateRegisterPage().CreateUser("User3", "User3@gmail.com", "12345", "54321"));
        }

        /* [Test]
         public void REPL()
         {
             new CreateRegisterPage().Repl2();
         } */
    }
}
