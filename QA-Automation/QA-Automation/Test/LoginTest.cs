﻿using NUnit.Framework;
using Xamarin.UITest;

namespace QAAutomation
{
    public class LoginTest : BaseTestFixture
    {
        public LoginTest(Platform platform)
            : base(platform)
        {
        }

        [Test]
        public void LoginExito()
        {
            Assert.IsTrue(true, "Username or password is incorrect", new LoginPage().Login("User1", "123"));
        }

        [Test]
        public void LoginContrasenaFail()
        {
            Assert.IsTrue(true, "Username or password is incorrect", new LoginPage().Login("User1", "456"));
        }

        [Test]
        public void LoginUsuarioFail()
        {
            Assert.IsTrue(true, "Username or password is incorrect", new LoginPage().Login("User2", "123"));
        }

        /* [Test]
         public void REPL()
         {
             new LoginPage().Repl2();
         }*/
    }
}
