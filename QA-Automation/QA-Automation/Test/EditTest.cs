﻿using NUnit.Framework;
using QAAutomation;
using Xamarin.UITest;


namespace QAAutomation
{
    public class EditTest : BaseTestFixture
    {
        public EditTest(Platform platform) : base(platform) { }

        [Test]
        public void EditComplete() 
        {
            Assert.IsTrue(true, "Enter all the values", new EditPage().Edituser("User4","theUser4@hotmail.com","654321","654321"));
        }

        [Test]
        public void EditEmptyValues()
        {
            Assert.IsEmpty("", "The Attributes of the user can't be empty", new EditPage().Edituser("", "", "", ""));
        }

        [Test]
        public void EditPasswordsNotMatch()
        {
            Assert.IsTrue(true, "The passwords need to match to update the user", new EditPage().Edituser("User4", "theUser4@hotmail.com", "654321", "123456"));
        }
    }
}
