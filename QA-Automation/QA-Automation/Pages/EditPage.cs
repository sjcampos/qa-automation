﻿using QAAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace QAAutomation
{
    public class EditPage : BasePage
    {
        readonly Query UserAux;
        readonly Query UName;
        readonly Query UEmail;
        readonly Query UPass;
        readonly Query UPassAgain;
        readonly Query EditButton;
        readonly Query EditViewButton;



        public EditPage()
        {
            if (OnAndroid)
            {
                UName = x => x.Marked("EuserU");
                UEmail = x => x.Marked("EemailU");
                UPass = x => x.Marked("EpassU");
                UPassAgain = x => x.Marked("EpassAU");
                UserAux = x => x.Marked("User1");
                EditViewButton = x => x.Marked("btnEdit");
                EditButton = x => x.Marked("btnSave");
            }
        }

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Marked("")
        };

        public EditPage Edituser(string user,string email, string password, string passTwo)
        {
            new LoginPage().Login("User2", "123");
            app.Tap(UserAux);
            app.Tap(EditViewButton);
            app.ClearText(UName);
            app.ClearText(UEmail);
            app.ClearText(UPass);
            app.ClearText(UPassAgain);
            app.EnterText(UName, user);
            app.EnterText(UEmail, email);
            app.EnterText(UPass, password);
            app.EnterText(UPassAgain, passTwo);
            app.Tap(EditButton);
            return this;
        }


    }
}
