﻿using System;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;


namespace QAAutomation
{
    public class LoginPage : BasePage
    {

        readonly Query loginbutton;
        readonly Query UserName;
        readonly Query UserPass;

        public LoginPage()
        {
            if (OnAndroid)
            {
                loginbutton = x => x.Marked("btnLogin");
                UserName = x => x.Marked("Euser");
                UserPass = x => x.Marked("Epass");
            }
        }

       /* public void Repl2()
        {
            app.Repl();
        }*/

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Marked("")
        };

        public LoginPage Login(String usuario, String pass)
        {
            app.EnterText(UserName, usuario);
            app.EnterText(UserPass, pass);
            app.Tap(loginbutton);
            return this;
        }
    }
}
