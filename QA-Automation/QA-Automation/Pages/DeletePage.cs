﻿using QAAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace QAAutomation
{
    public class DeletePage : BasePage
    {
        readonly Query User;
        readonly Query DeleteButton;
       


        public DeletePage()
        {
            if (OnAndroid)
            {
                User = x => x.Marked("User3");
                DeleteButton = x => x.Marked("btnDelete");

            }
        }

        /* public void Repl2()
         {
             app.Repl();
         }*/

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Marked("")
        };

        public DeletePage DeleteUser()
        {
            new LoginPage().Login("User1", "123");
            app.Tap(User);
            app.Tap(DeleteButton);

            return this;
        }


    }
}
