﻿using System;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;
namespace QAAutomation
{
    public class CreateRegisterPage : BasePage
    {

        readonly Query UserName;
        readonly Query UserEmail;
        readonly Query UserPass;
        readonly Query UserPassAgain;
        readonly Query CreaterUserButton;
        readonly Query AddUserButton;


        public CreateRegisterPage()
        {
            if (OnAndroid)
            {
                UserName = x => x.Marked("EuserU");
                UserEmail = x => x.Marked("EemailU");
                UserPass = x => x.Marked("EpassU");
                UserPassAgain = x => x.Marked("EpassAU");
                CreaterUserButton = x => x.Marked("btnSave");
                AddUserButton = x => x.Marked("btnNew");

            }
        }

        /* public void Repl2()
         {
             app.Repl();
         }*/

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Marked("")
        };

        public CreateRegisterPage CreateUser(String user, String email, String pass, String repass)
        {
            new LoginPage().Login("User1", "123");
            app.Tap(AddUserButton);
            app.EnterText(UserName, user);
            app.EnterText(UserEmail, email);
            app.EnterText(UserPass, pass);
            app.EnterText(UserPassAgain, repass);
            app.Tap(CreaterUserButton);
            return this;
        }
    }
}
