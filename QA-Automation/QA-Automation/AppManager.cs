﻿using NUnit.Framework;
using System;
using Xamarin.UITest;

namespace QAAutomation
{
    static class AppManager
    {
        //Esto cambia según la ubicación del archivo
        const string ApkPath = "C:/Users/Sebastian Campos/source/repos/qa-automation/QA-Automation/QA-Automation/APK/proyecto.apk";
        //
        static IApp app;
        public static IApp App
        {
            get
            {
                if (app == null)
                    //StartApp();
                    throw new NullReferenceException("AppManager.App' not set. Call 'AppManager.StartApp()' before trying to access it.");
                return app;
            }


        }
        //
        static Platform? platform;
        public static Platform Platform
        {
            get
            {
                if (platform == null)
                    //Platform = Platform.Android; 
                    throw new NullReferenceException("'AppManager.Platform' not set.");
                return platform.Value;
            }

            set
            {
                platform = value;
            }
        }
        //

        public static void StartApp()
        {
            if (Platform == Platform.Android)
            {
                app = ConfigureApp
                    .Android
                    // Used to run a .apk file:
                    .ApkFile(ApkPath)
                    .StartApp();
            }
        }
        //
    }
    //
}//